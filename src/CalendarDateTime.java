
public class CalendarDateTime extends CalendarDate{
	
	int month = 1; int day = 1; int year = 2012;
	int hour = 1; int minute = 0; int seconds = 0;
	
	public CalendarDateTime(int m, int d, int y, int h, int mi, int s)
	{
		super(m, d, y);
		
		hour = h; minute = mi; seconds = s;
	}
	
	public String toString()
	{
		var str_e = "AM";
		
		var hr = hour; if(hr > 12) { hr -= 12; str_e = "PM"; }
		
		var str_h = "" + hr; if(str_h.length() < 2) str_h = "0" + str_h;
		var str_m = "" + minute; if(str_m.length() < 2) str_m = "0" + str_m;
		var str_s = "" + seconds; if(str_s.length() < 2) str_s = "0" + str_s;
		
		return stringMonths[month - 1] + " " + day + ", " + year + " (" + str_h + ":" + str_m + ":" + str_s + " " + str_e + ")";
	}
}

import java.util.Random;

public class CalendarDateTester {
	
	public static void main(String args[])
	{
		int rptAmount = 3;
		Random r = new Random();
		for(int i = 0; i < rptAmount; i++)
		{
			CalendarDate d = new CalendarDateTime(r.nextInt(12) + 1, r.nextInt(31) + 1, r.nextInt(1500) + 1500, 
							r.nextInt(24), r.nextInt(60), r.nextInt(60));
			
			System.out.println(d.toString());
			System.out.println(d.nextDate());
			System.out.println(d.previousDate());
			System.out.println(d.dayOfWeek());
			
			System.out.println("--------------------------");
		}
	}
}


public class CalendarDate {
	
	int month = 1; int day = 1; int year = 2012;
	
	final int max_month = 12;
	int max_day = 31;
	
	String stringMonths[] = {"January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November","December"};
	
	CalendarDate(int m, int d, int y)
	{
		//Set Month
		if(m <= 12 && m > 0) this.month = m;
		
		//Set Day
		switch(this.month)
		{
			case 2:
				//Leap Year
				max_day = 28;
				if(y % 4 == 0 && y % 100 != 0)
				{
					max_day = 29;
				}
			break;
			
			case 4: max_day = 30; break;
			
			case 6: max_day = 30; break;
			
			case 9: max_day = 30; break;
			
			case 11: max_day = 30; break;
		}
		
		if(d <= max_day && d > 0) this.day = d;
		
		//Set Year
		this.year = y;
	}
	
	public String toString()
	{
		return stringMonths[month - 1] + " " + day + ", " + year;
	}
	
	public String nextDate()
	{
		int d = day + 1;
		int m = month;
		int y = year;
		
		if(d > max_day)
		{
			d = 1;
			
			m++;
			
			if(m > max_month)
			{
				y++;
			}
		}
		
		return stringMonths[m - 1] + " " + d + ", " + y;
	}
	
	private int daysInMonth(int m)
	{
		var dm = 31;
		
		switch(m)
		{
			case 2:
				//Leap Year
				dm = 28;
				if(year % 4 == 0 && year % 100 != 0)
				{
					dm = 29;
				}
			break;
			
			case 4: dm = 30; break;
			
			case 6: dm = 30; break;
			
			case 9: dm = 30; break;
			
			case 11: dm = 30; break;
		}
		
		return dm;
	}
	
	public String previousDate()
	{
		int d = day - 1;
		int m = month;
		int y = year;
		
		if(d <= 0)
		{
			m -= 1;
			if(m <= 0)
			{
				m = 12;
				y--;
			}
			
			int md = max_day;
			
			switch(m)
			{
				case 2:
					//Leap Year
					md = 28;
					if(y % 4 == 0)
					{
						md = 29;
					}
				break;
				
				case 4: md = 30; break;
				
				case 6: md = 30; break;
				
				case 9: md = 30; break;
				
				case 11: md = 30; break;
			}
			
			d = md;
		}
		
		return stringMonths[m - 1] + " " + d + ", " + y;
	}
	
	public String dayOfWeek()
	{
		String weekDay = "Sunday";
		
		int century = Integer.parseInt(Integer.toString(year).substring(0, 2));
		int digYear = Integer.parseInt(Integer.toString(year).substring(2, 4));
		
		int f = (int) (day + Math.floor((13*month - 1)/5) + digYear + Math.floor(digYear/4) + Math.floor(century/4) - 2*century);
		f++;
		
		switch(f % 7)
		{
			case 1: weekDay = "Monday"; break;
			case 2: weekDay = "Tuesday"; break;
			case 3: weekDay = "Wednesday"; break;
			case 4: weekDay = "Thursday"; break;
			case 5: weekDay = "Friday"; break;
			case 6: weekDay = "Saturday"; break;
		}
		
		return weekDay;
	}
	
	public int dateDifference(CalendarDate dt)
	{
		int dif = 0;
		
		if(this.year != dt.year)
		{
			dif += Math.abs(this.year - dt.year);
		}
		
		if(this.month != dt.month)
		{
			//TODO - make this accurate
			dif += Math.abs(this.month - dt.month);
		}
		
		if(this.day != dt.day)
		{
			dif += Math.abs(this.day - dt.day);
		}
		
		return dif;
	}
}
